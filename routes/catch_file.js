var express = require("express");
const serviceHost = require("../helper/service_host");
var router = express.Router();
const http = require("http");
const https = require("https");
var fs = require("fs");
var FormData = require("form-data");
const CommonHelper = require("../helper/common");
const fetch = require("node-fetch");

var multer = require("multer");
const appRoot = require("app-root-path");

var storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, appRoot + "/uploads");
  },
  filename: (req, file, callback) => {
    callback(null, file.originalname);
  },
});

var upload = multer({
  storage: storage,
}).single("file");

const unknownService = "unknown";
const unknownServiceMessage =
  "Unknown Service Destination, File Storage Service is ...";

router.get("/*", async function (req, res) {
  const requestUrl = req.url;

  const requestedService = serviceHost(requestUrl);

  if (requestedService.name === unknownService) {
    res.send({ message: unknownServiceMessage, data: ["huskar"] });
  }

  if (CommonHelper.isHttpsProtocol()) {
    https.get(`${requestedService}${req.url}`, (stream) => {
      stream.pipe(res);
    });
  } else {
    http.get(`${requestedService}${req.url}`, (stream) => {
      stream.pipe(res);
    });
  }
});

router.post("/*", upload, async (req, res) => {
  const requestUrl = req.url;
  const requestHeaders = req.headers;

  const requestedService = serviceHost(requestUrl);

  if (requestedService.name === unknownService) {
    res.send({ message: unknownServiceMessage, data: ["huskar"] });
  }
  
  try {
    const selectedFile = fs.createReadStream(req.file.path);

    let formData = new FormData();
    formData.append("file", selectedFile);

    const response  = await fetch(`${requestedService.host}${requestUrl}`, {
        method: 'POST',
        body: formData
    }).then(hasil => hasil.json()).catch(err => {
        console.error(err);
    });

    res.send(response);
  } catch (err) {
    res.send({ message: "Error Uploading File", error: err.message });
  }
});

module.exports = router;
