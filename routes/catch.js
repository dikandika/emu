var express = require('express');
const serviceHost = require('../helper/service_host');
var router = express.Router();
var CommonService = require('../services/common_service');


const unknownService = "unknown";
const unknownServiceMessage = "Unknown Service Destination";

router.get('/*', async function (req, res, next) {
    const requestUrl = req.url;

    const requestedService = serviceHost(requestUrl);

    if (requestedService.name === unknownService) {
        res.send({ message: unknownServiceMessage, data: [] });
    }

    const service = new CommonService(requestedService.host);
    const response = await service.get(req);

    res.status((response?.status ?? 500)).send((response?.data ?? {}));
});

router.post('/*', async function (req, res, next) {
    const requestUrl = req.url;

    const requestedService = serviceHost(requestUrl);

    if (requestedService.name === unknownService) {
        res.send({ message: unknownServiceMessage, data: [] });
    }

    const service = new CommonService(requestedService.host);
    const response = await service.post(req);

    res.send(response.data);
});

router.put('/*', async function (req, res, next) {
    const requestUrl = req.url;

    const requestedService = serviceHost(requestUrl);

    if (requestedService.name === unknownService) {
        res.send({ message: unknownServiceMessage, data: [] });
    }

    const service = new CommonService(requestedService.host);
    const response = await service.put(req);

    res.send(response.data);
});

router.patch('/*', async function (req, res, next) {
    const requestUrl = req.url;

    const requestedService = serviceHost(requestUrl);

    if (requestedService.name === unknownService) {
        res.send({ message: unknownServiceMessage, data: [] });
    }

    const service = new CommonService(requestedService.host);
    const response = await service.patch(req);

    res.send(response.data);
});

router.delete('/*', async function (req, res, next) {
    const requestUrl = req.url;

    const requestedService = serviceHost(requestUrl);

    if (requestedService.name === unknownService) {
        res.send({ message: unknownServiceMessage, data: [] });
    }

    const service = new CommonService(requestedService.host);
    const response = await service.delete(req);

    res.send(response.data);
});

module.exports = router;
