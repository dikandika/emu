const axios = require("axios");

class FileService {
  constructor(baseUrl) {
    this.baseUrl = baseUrl;
  }

  async post(req) {

    return await axios
      .post(`${this.baseUrl}${req.url}`, req.body, { headers: req.headers })
      .then(function (res) {
        return res;
      })
      .catch(function (err) {
        return err.response;
      });
      
  }
}

module.exports = FileService;
