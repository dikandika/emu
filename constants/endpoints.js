module.exports = class EndPoints {
    
    static owl = [
        "/api/v1/login-info",
        "/api/v1/pwdParam",
        "/api/v1/setForceChangePassword",
    ];
    
    static chip = [
        "/api/v1.0",
    ];
    
    static pigeon = [
        "/apiv1/vlinkMessage",
        "/apiv1/customerRegistration",
    ];
    
    static canary = [
        "/apiv1/globalLimit",
        "/apiv1/transactionLog",
        "/apiv1/transactionLog/audit",
        "/apiv1/deviceTrail",
        "/apiv1/sysparam",
    ];
    
    static eagle = [
        "/api/v1.0/master",
    ];
    
    static huskar = [
        "/huskar/image",
        "/huskar/upload",
    ];
    
    static lich = [
        "/lich",
    ]; 
    
    static omniknight = [
        "/omniknight/lovs",
        "/omniknight/lovs/categories",
        "/omniknight/lovs/dukcapil-param",
    ]; 
    
    static phoenix = [
        "/phoenix/customer",
        "/phoenix/log/customer",
        "/phoenix/history/customer",
    ]; 
    
    static harrier = [
        "/harrier/booking-quota",
        "/harrier/timetabled-calls"
    ]; 
    
    static abaddon = [
        "/abaddon/profession",
        "/abaddon/industry",
        "/abaddon/branch-office",
        "/abaddon/number-of-transactions-risk-question",
        "/abaddon/number-of-transactions-risk-score"
    ]; 
    
    static lifestealer = [
        "/lifestealer/special-person",
        "/lifestealer/special-person-mapping",
    ]; 
    
    static pudge = [
        "/pudge/profile",
    ]; 
    
    static bristleback = [
        "/bristleback",
    ]; 

}