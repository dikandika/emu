var indexRouter = require('./routes/index');
var catchRouter = require('./routes/catch');
var catchFileRouter = require('./routes/catch_file');

module.exports = (app) => {
    app.use('/', indexRouter);

    app.use('/emu/catch', catchRouter);
    app.use('/emu/catch_file', catchFileRouter);
};